﻿using Grpc.Core;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.GrpcService.Mappers;
using System;
using System.Linq;
using System.Threading.Tasks;



namespace Otus.Teaching.PromoCodeFactory.GrpcService
{
    public class CustomerService : CustomerGrpc.CustomerGrpcBase
    {
        private readonly ILogger<CustomerService> _logger;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        public CustomerService(ILogger<CustomerService> logger,
                                IRepository<Customer> customerRepository,
                                IRepository<Preference> preferenceRepository)
        {
            _logger = logger;
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public override async Task<CustomerListGrpcResponse> GetCustomersAsync(Empty request, ServerCallContext context)
        {
            var customers = await _customerRepository.GetAllAsync();
                        
            CustomerListGrpcResponse customerListGrpcResponse = new CustomerListGrpcResponse();

            foreach (var customer in customers)
            {
                customerListGrpcResponse.Customers.Add(CustomerGrpsMappers.MapToShortModel(customer));
            }

            return customerListGrpcResponse;
        }
        public override async Task<CustomerGrpcResponse> GetCustomerAsync(CustomerGrpcRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));
            if (customer == null)
                throw new RpcException(new Status(StatusCode.NotFound, "Customer not found"));
            return CustomerGrpsMappers.MapToModel(customer);
        }
        public override async Task<CustomerGrpcResponse> CreateCustomerAsync(CustomerGrpcCreateRequest request, ServerCallContext context)
        {
            var preferences = await _preferenceRepository
               .GetRangeByIdsAsync(request.PreferenceIds.Select(x => Guid.Parse(x)).ToList());

            Customer customer = CustomerGrpsMappers.MapFromModel(request, preferences);

            await _customerRepository.AddAsync(customer);

            return CustomerGrpsMappers.MapToModel(customer);
        }
        public override async Task<CustomerGrpcResponse> EditCustomerAsync(CustomerGrpcEditRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));

            if (customer == null)
                throw new RpcException(new Status(StatusCode.NotFound, "Customer not found"));

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds.Select(x => Guid.Parse(x)).ToList());

            CustomerGrpsMappers.MapFromModel(request, preferences, customer);

            await _customerRepository.UpdateAsync(customer);

            return CustomerGrpsMappers.MapToModel(customer);
        }
        public override async Task<Empty> DeleteCustomerAsync(CustomerGrpcRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));

            if (customer == null)
                throw new RpcException(new Status(StatusCode.NotFound, "Customer not found"));

            await _customerRepository.DeleteAsync(customer);

            return new Empty();
        }

    }
}
