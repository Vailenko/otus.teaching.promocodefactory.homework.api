﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.GrpcService.Mappers
{
    public class CustomerGrpsMappers
    {
        public static Customer MapFromModel(CustomerGrpcCreateRequest model, IEnumerable<Preference> preferences)
        {
            var customer = new Customer();
            customer.Id = Guid.NewGuid();


            customer.FirstName = model.FirstName;
            customer.LastName = model.LastName;
            customer.Email = model.Email;

            customer.Preferences = preferences.Select(x => new CustomerPreference()
            {
                CustomerId = customer.Id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();

            return customer;
        }

        public static Customer MapFromModel(CustomerGrpcEditRequest model, IEnumerable<Preference> preferences,
                                            Customer customer)
        {
            customer.FirstName = model.FirstName;
            customer.LastName = model.LastName;
            customer.Email = model.Email;

            customer.Preferences.Clear();
            customer.Preferences = preferences.Select(x => new CustomerPreference()
            {
                CustomerId = customer.Id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();

            return customer;
        }

        public static CustomerGrpcResponse MapToModel(Customer customer)
        {
            if (customer == null)
                return null;
            var model = new CustomerGrpcResponse();
            model.FirstName = customer.FirstName;
            model.LastName = customer.LastName;
            model.Id = customer.Id.ToString();
            model.Email = customer.Email;
            if (customer.Preferences != null)
            {
                model.Preferences.AddRange(customer.Preferences.Select(x => new PreferenceGrpcResponce()
                {
                    Id = x.PreferenceId.ToString(),
                    Name = x.Preference.Name

                }).ToList());
            }
            return model;
        }
        public static CustomerGrpcShortResponse MapToShortModel(Customer customer)
        {
            if (customer == null)
                return null;
            var model = new CustomerGrpcShortResponse();
            model.FirstName = customer.FirstName;
            model.LastName = customer.LastName;
            model.Id = customer.Id.ToString();
            model.Email = customer.Email;
            return model;
        }
    }
}
