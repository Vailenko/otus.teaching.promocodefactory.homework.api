﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.GqlService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.GqlService
{
    public class CustomerMapper
    {
        public static Customer MapFromModel(CreateCustomerInput model, IEnumerable<Preference> preferences = null)
        {
            var customer = new Customer();
            customer.Id = Guid.NewGuid();

            customer.FirstName = model.FirstName;
            customer.LastName = model.LastName;
            customer.Email = model.Email;

            if (preferences != null)
            {
                customer.Preferences = preferences.Select(x => new CustomerPreference()
                {
                    CustomerId = customer.Id,
                    Preference = x,
                    PreferenceId = x.Id
                }).ToList();
            }

            return customer;
        }
        public static Customer MapFromModel(EditCustomerInput model, IEnumerable<Preference> preferences = null, Customer customer = null)
        {

            customer.FirstName = model.FirstName;
            customer.LastName = model.LastName;
            customer.Email = model.Email;

            if (preferences != null)
            {
                customer.Preferences = preferences.Select(x => new CustomerPreference()
                {
                    CustomerId = customer.Id,
                    Preference = x,
                    PreferenceId = x.Id
                }).ToList();
            }

            return customer;
        }
    }
}
