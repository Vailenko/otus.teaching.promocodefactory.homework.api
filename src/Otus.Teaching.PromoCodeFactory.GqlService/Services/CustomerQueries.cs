﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HotChocolate;
using HotChocolate.Types;
using HotChocolate.Types.Relay;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.GqlService.Services
{
    [ExtendObjectType(Name = "Query")]
    public class CustomerQueries
    {
        /// <summary>
        /// Retrieve a customer by id.
        /// </summary>
        /// <param name="id">Customer id.</param>
        /// <param name="repository"></param>
        /// <returns>The customer.</returns>
        public async Task <Customer> GetCustomer(Guid id, [Service] IRepository<Customer> repository)
        {
            var customer = await repository.GetByIdAsync(id);
            return customer;
        }
        /// <summary>
        /// Retrieve customer list.
        /// </summary>
        /// <param name="repository"></param>
        /// <returns>The customers.</returns>
        [UsePaging]
        [UseFiltering]
        [UseSorting]
        public async Task<IEnumerable<Customer>> GetCustomers([Service] IRepository<Customer> repository)
        {
            var cusomers = await repository.GetAllAsync();
            return cusomers;
        }
       
    }
}
