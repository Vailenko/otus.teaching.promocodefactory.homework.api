﻿using System;
using System.Threading.Tasks;
using HotChocolate;
using HotChocolate.Subscriptions;
using HotChocolate.Types;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.GqlService.Models;

namespace Otus.Teaching.PromoCodeFactory.GqlService.Services
{
    [ExtendObjectType(Name = "Mutation")]
    public class CustomerMutations
    {
        /// <summary>
        /// Creates a new customer
        /// </summary>
        /// <param name="input"></param>
        /// <param name="customerRepository">Customer repository</param>
        /// <param name="preferenceRepository">Preference repository</param>
        /// <returns>The customer.</returns>
        public async Task<Customer> CreateCustomerAsync(
            CreateCustomerInput input,
            [Service] IRepository<Customer> customerRepository,
            [Service] IRepository<Preference> preferenceRepository)
        {

            var preferences = await preferenceRepository.GetRangeByIdsAsync(input.PreferenceIds);            

            Customer customer = CustomerMapper.MapFromModel(input, preferences);

            await customerRepository.AddAsync(customer);

            return customer;
        }
        /// <summary>
        /// Edit a customer
        /// </summary>
        /// <param name="input"></param>
        /// <param name="customerRepository">Customer repository</param>
        /// <param name="preferenceRepository">Preference repository</param>
        /// <returns>Updated customer.</returns>
        public async Task<Customer> EditCustomerAsync(
            EditCustomerInput input,
            [Service] IRepository<Customer> repository,
            [Service] IRepository<Preference> preferenceRepository)
        {
            var customer = await repository.GetByIdAsync(input.Id);

            if (customer == null)                
                  throw new Exception ("Customer not found");

            var preferences = await preferenceRepository.GetRangeByIdsAsync(input.PreferenceIds);

            CustomerMapper.MapFromModel(input, null, customer);

            await repository.UpdateAsync(customer);

            return customer;
        }

        /// <summary>
        /// Edit a customer
        /// </summary>
        /// <param name="repository">Customer repository</param>        
        /// <returns>Removed customer.</returns>
        public async Task<Customer> DeleteCustomerAsync(
            EditCustomerInput input,
            [Service] IRepository<Customer> repository)         
        {
            var customer = await repository.GetByIdAsync(input.Id);
            if (customer == null)
                throw new Exception("Customer not found");

            await repository.DeleteAsync(customer);

            return customer; 
        }


    }
}
